  //var
  const navBar = document.getElementsByTagName("nav")[0];
  const btnNav = document.querySelector(".fa-bars");
  const skillButton = document.getElementById("skills-id");
  btnNav.style.transform = "rotate(180deg)";
  // Eventlisteners
  btnNav.addEventListener("click", function () {
      if (btnNav.style.transform == "rotate(180deg)") {
          btnNav.style.transform = "rotate(270deg)";
          btnNav.style.top = "5%";
          navigationBar(" res", " res");
      } else {
          btnNav.style.top = "25%";
          btnNav.style.transform = "rotate(180deg)";
          navigationBar("", "drop-btn");
      }
  });
  // functions
  function navigationBar(classOne, classTwo) {
      if (navBar.className == "") {
          navBar.className += classOne;  
      } else {
          navBar.className = classOne;
      }
  }
